<?php

namespace app\controllers;

use app\models\emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmpleController implements the CRUD actions for emple model.
 */
class EmpleController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all emple models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => emple::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'emp_no' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single emple model.
     * @param int $emp_no Emp No
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($emp_no)
    {
        return $this->render('view', [
            'model' => $this->findModel($emp_no),
        ]);
    }

    /**
     * Creates a new emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new emple();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'emp_no' => $model->emp_no]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $emp_no Emp No
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($emp_no)
    {
        $model = $this->findModel($emp_no);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'emp_no' => $model->emp_no]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $emp_no Emp No
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($emp_no)
    {
        $this->findModel($emp_no)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $emp_no Emp No
     * @return emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($emp_no)
    {
        if (($model = emple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
